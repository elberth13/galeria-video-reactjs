import React from 'react';
import Media from './media.js';
import './playlist.css';
// import Play from '../../icons/components/play'
// import Pausa from '../../icons/components/pausa'
// import Volumen from '../../icons/components/volumen'
// import Fullscreen from '../../icons/components/fullscreen'

function Playlist(props){

    // const playlist = props.data.categories[0].playlist;
    // console.log(props.data);
    return(
      <div className="playlist">
        {/* <Play 
          size={25}
          color="red"
        />
        <Pausa 
          size={25}
          color="blue"
        />
        <Volumen 
          size={25}
          color="green"
        />
        <Fullscreen 
          size={25}
          color="orange"
        /> */}
        {
          props.playlist.map((item)=>{
            return <Media openModal={props.handleOpenModal} {...item} key={item.id}/>
          })
        }
        
      </div>
    )
}

export default Playlist;