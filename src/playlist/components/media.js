import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './media.css';

class Media extends PureComponent {
  state = {
    author: 'J. R. R. Tolkien'
  }
  // constructor(props){
  //   super(props)
  //   this.state ={
  //     author: props.author
  //   }
  // //   this.handClick = this.handClick.bind(this);
  // }
  // handClick = (event) => {
  //   //console.log(this.props.title);
  //   // this.setState({
  //   //   author: 'Elberth Bracho'
  //   // })
  //   console.log("entro");
  //   this.props.openModal(this.props);
  // }
  handleClick = (event) => {
    this.props.openModal(this.props);
  }
  render(){
    const style={
      container:{
        fontSize:14,
        backgroundColor:'white',
        color:'#44546b',
        cursor: 'pointer',
        width: 120,
        border: '1px solid blue'
      }
    }
    return (
      <div className="media" onClick={this.handleClick}>
        <div className="media-cover">
          <img
            src={this.props.cover}
            className="media-image"
            alt=""
            width={120}
            height={185}
          />
          <h3 className="media-title">{this.props.title}</h3>
          <p className="media-author">{this.props.director}</p>
        </div>
      </div>
    )
  }
}

Media.propTypes = {
  cover: PropTypes.string,
  title: PropTypes.string.isRequired,
  director: PropTypes.string,
  type: PropTypes.oneOf(['video', 'audio']),
}
export default Media;